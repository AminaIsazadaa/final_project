import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateVerification {

    @Test
    public void CheckingDateFormat() {

        System.setProperty("webdriver.chrome.driver", "/Users/macbookair/Documents/Iba/ibatech-homeworks/iba_final_project/src/main/resources/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://www.globalsqa.com/");
        Actions actions = new Actions(driver);

        try {
            DataPickerPage dataPickerPage = new DataPickerPage(driver);
            actions.moveToElement(dataPickerPage.testerHub).moveToElement(dataPickerPage.demoTestingSite).moveToElement(dataPickerPage.datePicker).click().build().perform();

            driver.switchTo().frame(dataPickerPage.datePickerIframe);

            dataPickerPage.datePickerInput.click();
            dataPickerPage.nextMonth.click();
            dataPickerPage.currentDay.click();


            String expected = dataPickerPage.datePickerInput.getAttribute("value");

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
            Date date = simpleDateFormat.parse(expected);
            Assert.assertTrue(expected.equals(simpleDateFormat.format(date)));



        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            driver.close();
        }

    }

}
