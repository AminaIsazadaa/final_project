import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import javax.swing.*;
import java.util.concurrent.TimeUnit;

public class ColumnNumberTest {

    @Test
    public void TheNumberofColumnItems() {

        System.setProperty("webdriver.chrome.driver", "/Users/macbookair/Documents/Iba/ibatech-homeworks/iba_final_project/src/main/resources/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://www.globalsqa.com/");
        Actions actions = new Actions(driver);

        try {
            MenuItemsPage menuItemsPage = new MenuItemsPage(driver);
            actions.moveToElement(menuItemsPage.testerHub).moveToElement(menuItemsPage.demoTestingSite).click().build().perform();

            int expectedNumberOfColumnItem = 6;
            Assert.assertTrue(menuItemsPage.firstColumn.size() == expectedNumberOfColumnItem && menuItemsPage.secondColumn.size() == expectedNumberOfColumnItem && menuItemsPage.thirdColumn.size() == expectedNumberOfColumnItem);

        } finally {
            driver.close();
        }

    }
}

