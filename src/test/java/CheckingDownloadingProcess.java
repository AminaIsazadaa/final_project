import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class CheckingDownloadingProcess {

    @Test
    public void CheckingDownlaoding (){


        System.setProperty("webdriver.chrome.driver", "/Users/macbookair/Documents/Iba/ibatech-homeworks/iba_final_project/src/main/resources/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://www.globalsqa.com/");
        Actions actions = new Actions(driver);

        WebDriverWait webDriverWait = new WebDriverWait(driver, 20);


        try {
            DownloadPage downloadPage = new DownloadPage(driver);
            actions.moveToElement(downloadPage.testerHub).moveToElement(downloadPage.demoTestingSite).moveToElement(downloadPage.progressBar).click().build().perform();

            driver.switchTo().frame(downloadPage.downloadButtonIframe);
            downloadPage.downloadButton.click();

            String expectedWordAfterDownloading = "Complete!";
            Assert.assertTrue(webDriverWait.until(ExpectedConditions.textToBe(downloadPage.completeWord, expectedWordAfterDownloading)));


        } finally {
            driver.close();
        }

    }
}
