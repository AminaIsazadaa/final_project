import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class PageObjectPattern {

    WebDriver driver;
    public PageObjectPattern(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

}
