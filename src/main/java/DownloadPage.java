import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DownloadPage extends PageObjectPattern {

    public DownloadPage(WebDriver driver) {

        super(driver);

    }

    @FindBy(xpath = "//*[@id=\"menu-item-2822\"]/a")
    public WebElement testerHub;

    @FindBy(xpath = "//*[@id=\"menu-item-2823\"]/a")
    public WebElement demoTestingSite;

    @FindBy(xpath = "//*[@id=\"menu-item-2832\"]/a/span")
    public WebElement progressBar;

    @FindBy(css = ".demo-frame.lazyloaded")
    public WebElement downloadButtonIframe;

    By completeWord = By.xpath("//*[@id=\"dialog\"]/div[1]");
    //public WebElement completeWord;

    @FindBy(xpath = "//*[@id=\"downloadButton\"]")
    public WebElement downloadButton;

}
