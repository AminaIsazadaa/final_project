import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DataPickerPage extends PageObjectPattern {


    public DataPickerPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"menu-item-2822\"]/a")
    public WebElement testerHub;

    @FindBy(xpath = "//*[@id=\"menu-item-2823\"]/a")
    public WebElement demoTestingSite;

    @FindBy(xpath = "//*[@id='menu']/ul/li[4]/div/ul/li/div/ul/li[@id='menu-item-2827']")
    public WebElement datePicker;

    @FindBy(xpath = "//*[@id=\"post-2661\"]/div[2]/div/div/div[1]/p/iframe")
    public WebElement datePickerIframe;

    @FindBy(xpath = "//input[@id=\"datepicker\"]")
    public WebElement datePickerInput;

    @FindBy(xpath = "//*[@id=\"ui-datepicker-div\"]/div/a[2]")
    public WebElement nextMonth;

    @FindBy(xpath = "//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[1]/td[1]/a")
    public WebElement currentDay;





}
