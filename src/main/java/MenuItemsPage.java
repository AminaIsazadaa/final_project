import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MenuItemsPage extends PageObjectPattern {

    public MenuItemsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"menu-item-2822\"]/a")
    public WebElement testerHub;

    @FindBy(xpath = "//*[@id=\"menu-item-2823\"]/a")
    public WebElement demoTestingSite;

    @FindBy(xpath = "//div[@class='price_column '][1]/ul/li[@class='price_footer']")
    List<WebElement> firstColumn;

    @FindBy(xpath = "//div[@class='price_column '][2]/ul/li[@class='price_footer']")
    List<WebElement> secondColumn;

    @FindBy(xpath = "//div[@class='price_column '][3]/ul/li[@class='price_footer']")
    List<WebElement> thirdColumn;


}
